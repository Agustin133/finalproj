# Ecommerce

## Description
This is a back end project about an e-commerce application. This app is based on MVC architecture using domaindomain driven design and as databases uses MySql MariaDb.
This application contains a login mechanism based in JWT(Json Web Token), a shopping cart, purchase order generator and sending the respective orders to the user by mail, as validator uses Joi and for sending mails uses Nodemailer. For the Front-End uses the Pug template engine.

## EndPoints
This application contains the following endpoints:
[ecommerceSuagger.com](https://app.swaggerhub.com/apis/Agustin133/Ecommerce/1.0.0)

## Heroku
This application deploys on heroku:
[ecommerceHeroku.com](https://ecommercefinal1.herokuapp.com/)

## Scripts
```
npm run start
npm run linter

```

## Usage
This application uses the following libraries

```
"bcrypt": "^5.0.1",
"body-parser": "^1.19.0",
"eslint": "^5.16.0",
"eslint-config-google": "^0.14.0",
"eslint-config-prettier": "^6.5.0",
"eslint-plugin-prettier": "^3.1.1",
"express": "^4.17.1",
"express-http-context": "^1.2.4",
"joi": "^17.4.0",
"jsonwebtoken": "^8.5.1",
"knex": "^0.95.2",
"log4js": "^6.3.0",
"mysql": "^2.18.1",
"nodemailer": "^4.7.0",
"nodemon": "^2.0.7",
"pug": "^3.0.2",
"socket.io": "^3.1.2",
"sql": "^0.78.0"

```
