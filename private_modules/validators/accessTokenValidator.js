const jwt = require('jsonwebtoken');
const tokenSecret = 'my-token-secret';
const httpContext = require('express-http-context');

//this function generate a user token
function generateToken(user){
    return jwt.sign({user: user}, tokenSecret);
};

//this function verify the chat token 
async function verifyTokenChat(token){
    if(token){
        try {
            await jwt.verify(token, tokenSecret);
            const tokenDecoded = await jwt.decode(token, tokenSecret);
            return tokenDecoded;
        } catch (error) {
            return 'Unauthorized'
        }
    } else {
        return 'Unauthorized'
    };
};

// this function verify token 
async function verifyToken(req, res, next){
    const token = req.header('Authorization');
    if(token){
        if(token.startsWith("Bearer ")){
            const tokenSplited = token.split(" ");
            try {
                await jwt.verify(tokenSplited[1], tokenSecret);
                const tokenDecoded = await jwt.decode(tokenSplited[1], tokenSecret);
                // httpContext.ns.bindEmitter(req);
                // httpContext.ns.bindEmitter(res);
                httpContext.set('userToken', tokenDecoded);
                next();
            } catch (error) {
                res.status(401).json({ message: 'Unauthorized'});
            }
        }else {
            res.status(401).json({ message: 'Unauthorized'});
        }
    } else {
        res.status(401).json({ message: 'Unauthorized'});
    };
};

//this function verify user role
async function validateUserRole(req, res, next) {
    const token = req.header('Authorization');
    if(token){
        if(token.startsWith("Bearer ")){
            const tokenSplited = token.split(" ");
            try {
                await jwt.verify(tokenSplited[1], tokenSecret);
                const tokenDecoded = await jwt.decode(tokenSplited[1], tokenSecret);
                if (tokenDecoded.user.user_admin != 1){
                    throw 'error';
                }
                next();
            } catch (error) {
                res.status(401).json({ message: 'Unauthorized'});
            }
        }else {
            res.status(401).json({ message: 'Unauthorized'});
        }
    } else {
        res.status(401).json({ message: 'Unauthorized'});
    };
}

module.exports = {
    verifyToken,
    generateToken,
    validateUserRole,
    verifyTokenChat
}
