const nodemailer = require('nodemailer');
//require('dotenv').config();

function transporter() {
    const nodemailertransport = nodemailer.createTransport({
        host: 'smtp.gmail.com',
        port: 587,
        secure: false,
        requireTLS: true,
        auth: {
          user: 'obertiagustin6@gmail.com',
          pass: 'agustinoberti775'
        }
    });
    return nodemailertransport;
}

function mailOptions(data, mail) {
    const dataToSend = data
    const mailoptions = {
        from: 'Servidor Node.js',
        to: mail,
        subject: 'Buy Order',
        html: ` <h1>New buy order </h1>
                <h2>Thanks for your purchase !!!</h2>
                <h4>Status: ${dataToSend.condition}</h4>
                <ul>
                    <li>Total: $${dataToSend.total}</li>
                    <li>Date: ${dataToSend.created_on}</li>
                </ul>
                <h4>Delivery direction: </h4>
                <ul>
                    <li>Street: ${dataToSend.delivery_direction.street}</li>
                    <li>House number: ${dataToSend.delivery_direction.house_number}</li>
                    <li>Postal code: ${dataToSend.delivery_direction.postal_code}</li>
                    <li>Floor: ${dataToSend.delivery_direction.floor}</li>
                    <li>Department: ${dataToSend.delivery_direction.department}</li>
                </ul><br>`,
    }
    return mailoptions;
}

function cartOptions(data, mail) {
    const dataToSend = data
    const mailoptions = {
        from: 'Servidor Node.js',
        to: mail,
        subject: 'Order products',
        html: `<h1>New purchase order </h1>
            <h4>Status: ${dataToSend.order_status}</h4>
            <h2>This are your products</h2>
            <ul>
                <li>${JSON.stringify(dataToSend.products)}</li>
            </ul><br>
            <h4>Total: ${dataToSend.total}</h4>`,
    }
    return mailoptions;
}

module.exports = {
    transporter,
    mailOptions,
    cartOptions
}