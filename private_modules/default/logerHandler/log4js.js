const log4js = require('log4js');

// This is the error logger
function log() {
    const loger = log4js.configure({
        appenders: {
            console: {type: 'console'},
            fileWarn: {type: 'file', filename: './private_modules/default/logerHandler/warnFile'},
            fileErr: {type: 'file', filename: './private_modules/default/logerHandler/errorFile'}
        },
        categories: {
            default: {appenders: ['console'], level: 'trace'},
            fileWarn: {appenders: ['fileWarn'], level:'warn'},
            fileErr: {appenders: ['fileErr'], level: 'error'}
        }
    })
    return loger;
};

module.exports = {
    log
}