const express = require('express');
const bodyParser = require('body-parser');
const router = require('./application/routes/index');
const path = require('path');
const log4js = require('../private_modules/default/logerHandler/log4js');
const PORT = process.env.PORT || 3000;
const httpContext = require('express-http-context');

const logger = log4js.log();
const loggerConsole = logger.getLogger();

const app = express();

app.use(bodyParser.json());
app.use(httpContext.middleware);

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use('/', router);

app.get('/', (req, res) => {
  res.render('index');
});

const server = app.listen(PORT, function () {
  loggerConsole.info(`Server running on: http://localhost:${PORT}`);
  const socket = require('../src/infrastructure/sockets/chat');
  socket(server);
});
